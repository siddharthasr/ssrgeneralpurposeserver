/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssrgeneralpurposeserver.backend;

import java.net.Socket;
import java.util.HashMap;
import ssrcommon.database.SSRBasicDBOperationsHandler;
import ssrcommon.threads.SSRBasicBackendRequest;
import ssrcommon.threads.SSRBasicBackendResponse;
import ssrcommon.threads.SSRBasicBackendServicesWorkerThread;

/**
 * Created on 7/12/2016.
 */
public class SSRGPServerWorkerThread extends SSRBasicBackendServicesWorkerThread
{   
    public static final String RUN_CUSTOM_QUERY = "RUN_CUSTOM_QUERY" ;
    public static final String RUN_CUSTOM_DML = "RUN_CUSTOM_DML" ;
    
    public static final String QUERY_PARAM_NAME = "QUERY_PARAM_NAME" ;
    
    public SSRGPServerWorkerThread(Socket i_socketToGiveServiceTo) throws Exception
    {
        super(i_socketToGiveServiceTo) ;
    }
    
    @Override
    protected void processClientRequest() throws Exception
    {
        System.out.println("Entering SSRGPServerWorkerThread.processClientRequest") ;
        
        String clientRequestCode = ((SSRBasicBackendRequest)this.m_clientRequest).m_serviceRequestCode ;
        switch(clientRequestCode)
        {
            case SSRGPServerWorkerThread.RUN_CUSTOM_QUERY :
            {
                this.handleRunCustomQuery() ;
                break ;
            }
            case SSRGPServerWorkerThread.RUN_CUSTOM_DML :
            {
                this.handleRunCustomDML() ;
                break ;
            }
            default :
            {
                System.out.println("Unrecognized client request code : " + clientRequestCode) ;
                throw new Exception("Unrecognized client request code : " + clientRequestCode) ;
            }
        }
        System.out.println("Response to client : " + this.m_responseToClient.toString()) ;
        return ;
    }
    
    private void handleRunCustomQuery() throws Exception
    {
        this.logMessage("Entering SSRGPServerWorkerThread.handleRunCustomQuery") ;
        SSRBasicBackendRequest clientRequest = (SSRBasicBackendRequest) this.m_clientRequest ;
        HashMap<String, Object> queryParams = (HashMap<String, Object>)clientRequest.m_requestData ;
        String queryToRun = (String)queryParams.get(SSRGPServerWorkerThread.QUERY_PARAM_NAME) ;
        SSRBasicDBOperationsHandler dbOperationsHandlerForQuery = new SSRBasicDBOperationsHandler() ;
        this.prepareStandardSuccessfulResponse() ;
        SSRBasicBackendResponse responseToClient = (SSRBasicBackendResponse)this.m_responseToClient ;
        responseToClient.m_responseData = dbOperationsHandlerForQuery.runCustomQuery(queryToRun) ;
        this.writeResponseToSocket() ;
    }
    
    private void handleRunCustomDML() throws Exception
    {
        this.logMessage("Entering SSRGPServerWorkerThread.handleRunCustomDML") ;
        SSRBasicBackendRequest clientRequest = (SSRBasicBackendRequest) this.m_clientRequest ;
        HashMap<String, Object> queryParams = (HashMap<String, Object>)clientRequest.m_requestData ;
        String dmlQueryToRun = (String)queryParams.get(SSRGPServerWorkerThread.QUERY_PARAM_NAME) ;
        SSRBasicDBOperationsHandler dbOperationsHandlerForDML = new SSRBasicDBOperationsHandler() ;
        Integer updatedRows = dbOperationsHandlerForDML.runCustomDml(dmlQueryToRun) ;
        this.prepareStandardSuccessfulResponse();
        SSRBasicBackendResponse responseForClient = (SSRBasicBackendResponse)this.m_responseToClient ;
        responseForClient.m_responseData = updatedRows ;
        responseForClient.m_responseDataSize = 1 ;
        this.writeResponseToSocket() ;
    }
}
