/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssrgeneralpurposeserver.backend;

import java.net.Socket;
import ssrcommon.threads.SSRBasicBackendServicesSocketServer;
import ssrcommon.threads.SSRBasicBackendServicesWorkerThread;

/**
 * Created on 7/12/2016.
 */
public class SSRGPServer extends SSRBasicBackendServicesSocketServer 
        implements Cloneable
{
    public static final String SERVER_IP_ADDRESS = "159.203.132.191" ;
    public static final Integer SERVER_PORT = 8200 ;
    public static final Integer CONTROL_PORT = 8210 ;
    
    public SSRGPServer(Integer i_serverPort, Integer i_serverControlPort) 
            throws Exception
    {
        super(i_serverPort, i_serverControlPort) ;
    }
    
    protected SSRBasicBackendServicesWorkerThread getOneWorkerThread(Socket i_clientSocket)
            throws Exception
    {
        return new SSRGPServerWorkerThread(i_clientSocket) ;
    }
}
